const React = require('react')
const ReactRouter = require('react-router')
const { Link } = ReactRouter
const Store = require('./Store.jsx')
const { connector } = Store

class Header extends React.Component {
  constructor (props) {
    super(props)

    this.handleSearchTermChange = this.handleSearchTermChange.bind(this)
  }
  handleSearchTermChange (e) {
    this.props.setSearchTerm(e.target.value)
  }
  renderUtilSpace () {
    if (this.props.showSearch) {
      return (
        <input type="text" className="search-input" placeholder="Buscar" value={this.props.searchTerm} onChange={this.handleSearchTermChange} />
      )
    }

    return (
      <h2 className="header-back">
        <Link to="/search">Voltar</Link>
      </h2>
    )
  }
  render () {
    const utilSpace = this.renderUtilSpace()
    return (
      <header className="header">
        <h1 className="brand">
          <Link to="/" className="brand-link">FletNix</Link>
        </h1>
        {utilSpace}
      </header>
    )
  }
}

Header.propTypes = {
  setSearchTerm: React.PropTypes.func,
  showSearch: React.PropTypes.bool,
  searchTerm: React.PropTypes.string
}

module.exports = connector(Header)
