const React = require('react')
const Header = require('./Header.jsx')
const Store = require('./Store.jsx')
const { connector } = Store
const axios = require('axios')

class Details extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      imdbData: {}
    }
  }
  componentDidMount () {
    const imdbID = this.props.shows[this.props.params.id].imdbID
    console.log(imdbID)
    axios
      .get(`http://www.omdbapi.com/?i=${imdbID}`)
      .then((response) => {
        this.setState({ imdbData: response.data })
      })
      .catch((error) => {
        console.error('axios error', error)
      })
  }
  render () {
    const params = this.props.params || {}
    const { title, description, year, poster, trailer } = this.props.shows[params.id]
    let rating
    if (this.state.imdbData.imdbRating) {
      rating = <h3 className="video-rating">{this.state.imdbData.imdbRating}</h3>
    }
    return (
      <div className="container">
        <Header />
        <div className="video-info">
          <h1 className="video-title">{title}</h1>
          <h2 className="video-year">{year}</h2>
          {rating}
          <img src={`/public/img/posters/${poster}`} alt="poster" className="video-poster" />
          <p className="video-description">{description}</p>
        </div>
        <div className="video-container">
          <iframe
            src={`https://www.youtube-nocookie.com/embed/${trailer}?rel=0&amp;showinfo=0`}
            frameBorder="0"
            allowFullScreen></iframe>
        </div>
      </div>
    )
  }
}

Details.propTypes = {
  params: React.PropTypes.object,
  route: React.PropTypes.object,
  shows: React.PropTypes.arrayOf(React.PropTypes.object)
}

module.exports = connector(Details)
