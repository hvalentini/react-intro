const React = require('react')
const ShowCard = require('./ShowCard.jsx')
// const data = require('../public/data.json')
const findIndex = require('lodash.findindex')
const Header = require('./Header.jsx')
const Store = require('./Store.jsx')
const { connector } = Store

class Search extends React.Component {
  constructor (props) {
    super(props)
    this.handleDeleteShow = this.handleDeleteShow.bind(this)
  }

  handleDeleteShow (imdbID) {
    const idx = findIndex(this.state.data, { 'imdbID': imdbID })
    const newShows = this.state.data.slice()
    newShows.splice(idx, 1)
    // console.log(idx)
    this.setState({ data: newShows })
  }

  render () {
    return (
      <div className="container">
        <Header showSearch />
        <div className="shows">
          {
            this.props.shows
            .filter((show) => {
              const str1 = `${show.title} ${show.description}`.toUpperCase()

              const str2 = this.props.searchTerm.toUpperCase()
              return str1.indexOf(str2) >= 0
            })
            .map((show, index) => (
              <ShowCard
                {...show}
                key={index}
                id={index}
                onDeleteClick={this.handleDeleteShow}
              />
            ))
          }
        </div>
      </div>
    )
  }
}

Search.propTypes = {
  route: React.PropTypes.object,
  searchTerm: React.PropTypes.string,
  shows: React.PropTypes.arrayOf(React.PropTypes.object)
}

module.exports = connector(Search)
